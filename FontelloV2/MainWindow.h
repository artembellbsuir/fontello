#pragma once

#include <windows.h>
#include "BaseWindow.h"
#include <string>

class MainWindow : public BaseWindow<MainWindow>
{
private:
	std::wstring m_text;
	HFONT m_hfont;
	HBRUSH m_hbrush;
	LOGFONT m_logfont;

public:
	PCWSTR  ClassName() const { return L"Sample Window Class"; }
	LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);

	MainWindow();

	void OnCreate();
	void OnPaint();
	void OnKeydown(UINT param);
	void OnCharacter(UINT param);
	void OnGetMinMaxInfo(UINT param);
	void OnSize(UINT param);
	void ChangeFontSize();
};
