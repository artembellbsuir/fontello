#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include <windows.h>
#include "MainWindow.h"

#include <tchar.h>
#include <stdio.h>
#include <string>

const int wndMinHeight = 450;
const int wndMinWidth = 800;
const int cols = 3, rows = 3;

LRESULT MainWindow::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_CREATE: {
		OnCreate();
		return 0;
	}
	case WM_DESTROY: {
		PostQuitMessage(0);
		return 0;
	}
	case WM_PAINT:
	{
		OnPaint();
		return 0;
	}
	case WM_CHAR: {
		OnCharacter(wParam);
		return 0;
	}
	case WM_KEYDOWN: {
		OnKeydown(wParam);
		return 0;
	}
	case WM_SIZE: {
		OnSize(lParam);
		return 0;
	}
	case WM_GETMINMAXINFO: {
		OnGetMinMaxInfo(lParam);
		return 0;
	}


	default:
		return DefWindowProc(m_hwnd, uMsg, wParam, lParam);
	}
	return TRUE;
}

MainWindow::MainWindow()
{
	m_text = L"abcdefafa gpbotiqa912r40avaef 01podm";
	m_hbrush = NULL;
	m_hfont = NULL;
	m_logfont = {};
}

void MainWindow::OnCreate()
{
	RECT rect;
	GetClientRect(m_hwnd, &rect);

	m_logfont.lfCharSet = DEFAULT_CHARSET;
	m_logfont.lfPitchAndFamily = FIXED_PITCH;

	const TCHAR* fontName = TEXT("Comic Sans MS");
	_tcscpy_s(m_logfont.lfFaceName, fontName);

	m_logfont.lfWeight = 400;
	m_logfont.lfEscapement = 0;
	m_logfont.lfItalic = 0;
	m_logfont.lfUnderline = 0;
	m_logfont.lfStrikeOut = 0;

	ChangeFontSize();
	
	m_hbrush = CreateSolidBrush(RGB(0, 0, 0));
}

void MainWindow::OnPaint()
{

	RECT  rect;
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(m_hwnd, &ps);

	FillRect(hdc, &ps.rcPaint, (HBRUSH)(COLOR_WINDOW + 1));

	GetClientRect(m_hwnd, &rect);

	LONG windowWidth = rect.right,
		windowHeight = rect.bottom,
		currentX = 0, currentY = 0,
		baseFrameWidth = windowWidth / cols,
		baseFrameHeight = windowHeight / rows;

	SelectObject(hdc, m_hfont);

	for (int i = 0; i < cols; i++) {
		for (int j = 0; j < rows; j++) {
			RECT textRect;
			SetRect(&textRect, currentX, currentY, currentX + baseFrameWidth, currentY + baseFrameHeight);
			SetTextColor(hdc, RGB(0xFF, 0x00, 0x00));
			DrawText(hdc, m_text.c_str(), -1, &textRect, DT_CENTER | DT_WORDBREAK);
			FrameRect(hdc, &textRect, m_hbrush);
			currentX += baseFrameWidth;
		}
		currentX = 0;
		currentY += baseFrameHeight;
	}

	EndPaint(m_hwnd, &ps);
}


void MainWindow::OnGetMinMaxInfo(UINT param)
{
	LPMINMAXINFO lpMinMaxInfo = (LPMINMAXINFO)param;
	lpMinMaxInfo->ptMinTrackSize.x = wndMinWidth;
	lpMinMaxInfo->ptMinTrackSize.y = wndMinHeight;
}

void MainWindow::OnSize(UINT param)
{
	ChangeFontSize();

	InvalidateRect(m_hwnd, NULL, FALSE);
}

void MainWindow::ChangeFontSize()
{
	RECT rect;
	GetClientRect(m_hwnd, &rect);
	LONG windowWidth = rect.right,
		windowHeight = rect.bottom,
		baseFrameWidth = windowWidth / cols,
		baseFrameHeight = windowHeight / rows;


	LONG oldWidth = m_logfont.lfWidth;
	
	m_logfont.lfWidth = baseFrameWidth / m_text.length();
	if (m_logfont.lfWidth > 50) {
		m_logfont.lfWidth = oldWidth;
	}
	m_logfont.lfHeight = m_logfont.lfWidth * 3;

	m_hfont = CreateFontIndirect(&m_logfont);
}

void MainWindow::OnKeydown(UINT param)
{
	WPARAM key = param;

	switch (key) {
	case VK_BACK: {
		if (m_text.length() > 1) {
			m_text = m_text.substr(0, m_text.length() - 1);
		}
	}
	}
	ChangeFontSize();

	InvalidateRect(m_hwnd, NULL, FALSE);
}

void MainWindow::OnCharacter(UINT param)
{
	UINT letter = param;
	if ('\b' != letter) {
		char buf1[] = { letter };
		std::wstring ws(&buf1[0], &buf1[1]);
		m_text += ws;
	}
}

